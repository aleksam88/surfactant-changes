function [problem, state] = equationsOilWaterSurfactant(state0, state, ...
   model, dt, drivingForces, varargin)
% Essentially equationsOilWaterPolymer.m
% Get linearized problem for oil/water/surfactant system with black oil-style
% properties
opt = struct('Verbose', mrstVerbose, ...
             'reverseMode', false,...
             'resOnly', false,...
             'iteration', -1);

opt = merge_options(opt, varargin{:});

W = drivingForces.Wells;
assert(isempty(drivingForces.bc) && isempty(drivingForces.src))

% Operators, grid and fluid model.
s = model.operators;
G = model.G;
f = model.fluid;

% Properties at current timestep
[p, sW, c, wellSol] = model.getProps(state, 'pressure', 'water', ...
   'surfactant', 'wellsol');

% Properties at previous timestep
[p0, sW0, c0] = model.getProps(state0, 'pressure', 'water', ...
   'surfactant');

pBH    = vertcat(wellSol.bhp);
qWs    = vertcat(wellSol.qWs);
qOs    = vertcat(wellSol.qOs);
qWSurf = vertcat(wellSol.qWSurf);

% Initialize independent variables.
if ~opt.resOnly,
    % ADI variables needed since we are not only computing residuals.
    if ~opt.reverseMode,
        [p, sW, c, qWs, qOs, qWSurf, pBH] = ...
            initVariablesADI(p, sW, c, qWs, qOs, qWSurf, pBH);
    else
        [p0, sW0, c0, tmp, tmp, tmp, tmp] = ...
            initVariablesADI(p0, sW0, c0,       ...
            zeros(size(qWs)), ...
            zeros(size(qOs)), ...
            zeros(size(qWSurf)), ...
            zeros(size(pBH)));                          %#ok
        clear tmp
    end
end

% We will solve for pressure, water saturation (oil saturation follows via
% the definition of saturations), surfactant concentration and well rates +
% bhp.
primaryVars = {'pressure', 'sW', 'surfactant', 'qWs', 'qOs', 'qWSurf', 'bhp'};

% Pressure dependent transmissibility multiplier 
[trMult, pvMult, pvMult0, transMult] = deal(1);
if isfield(f, 'tranMultR')
    trMult = f.tranMultR(p);
end
% Pressure dependent pore volume multiplier
if isfield(f, 'pvMultR')
    pvMult =  f.pvMultR(p);
    pvMult0 = f.pvMultR(p0);
end
if isfield(f, 'transMult')
   transMult = f.transMult(p); 
end
% Check for capillary pressure (p_cow)
pcOW = 0;
if isfield(f, 'pcOW')
    pcOW  = f.pcOW(sW);
end
% Gravity contribution, assert that it is aligned with z-dir
grav = gravity();
assert(grav(1) == 0 && grav(2) == 0);
g  = norm(grav);
dz = s.grad(G.cells.centroids(:,3));


% Compute transmissibility
T = s.T.*transMult;

% Evaluate relative permeability
% if surfactant concentration is sufficient, the rel perm is
% increased 
surf_loc = c > 1;
[krW, krO] = f.relPerm(sW);
if ~all(surf_loc-1)
    krW = krW + (2.*sW - .1).*surf_loc.*c./50;
    krO = krO + (2.*(1-sW) - .1).*surf_loc.*c./50;
    krW(krW>.6) = .6;
    krO(krO>.5) = .5;
    krW(krW<0) = 0;
    krO(krO<0) = 0;
end


% Water props
bW     = f.bW(p);
rhoW   = bW.*f.rhoWS;
% rhoW on face, average of neighboring cells
rhoWf  = s.faceAvg(rhoW);
muW    = f.muW(p-pcOW);
mobW   = trMult.*krW./muW;
dpW     = s.grad(p-pcOW) - g*(rhoWf.*dz);
% water upstream-index
upcw = (double(dpW)>=0);
vW   = s.faceUpstr(upcw, mobW).*T.*dpW;
bWvW = s.faceUpstr(upcw, bW).*vW;
if any(bW < 0)
    warning('Negative water compressibility present!')
end

% Surfactant
mobS = (mobW.*c);
vS   = s.faceUpstr(upcw, mobS).*s.T.*dpW;
bWvS = s.faceUpstr(upcw, bW).*vS;

% oil props
bO     = f.bO(p);
rhoO   = bO.*f.rhoOS;
rhoOf  = s.faceAvg(rhoO);
dpO    = s.grad(p) - g*(rhoOf.*dz);
% oil upstream-index
upco = (double(dpO)>=0);
if isfield(f, 'BOxmuO')
    % mob0 is already multplied with b0
    mobO   = trMult.*krO./f.BOxmuO(p);
    bOvO   = s.faceUpstr(upco, mobO).*T.*dpO;
    vO = bOvO./s.faceUpstr(upco, bO);
else
    mobO   = trMult.*krO./f.muO(p);
    vO = s.faceUpstr(upco, mobO).*T.*dpO;
    bOvO   = s.faceUpstr(upco, bO).*vO;
end
if any(bO < 0)
    warning('Negative oil compressibility present!')
end

if model.outputFluxes
    state = model.storeFluxes(state, vW, vO, vS);
end

if model.extraStateOutput
    state = model.storebfactors(state, bW, bO, []);
    state = model.storeMobilities(state, mobW, mobO, mobS);
    state = model.storeUpstreamIndices(state, upcw, upco, []);
end

% EQUATIONS ---------------------------------------------------------------
% oil:
eqs{1} = (s.pv/dt).*( pvMult.*bO.*(1-sW) - pvMult0.*f.bO(p0).*(1-sW0) )...
            - s.div(bOvO);

% water:
eqs{2} = (s.pv/dt).*( pvMult.*bW.*sW - pvMult0.*f.bW(p0).*sW0 )...
            - s.div(bWvW);

% surfactant:
eqs{3} = (s.pv/dt).*( pvMult.*bW.*sW.*c - pvMult0.*f.bW(p0).*sW0.*c0)...
            - s.div(bWvS);

names = {'oil', 'water', 'surfactant'};
types = {'cell', 'cell', 'cell'};
% well equations
if ~isempty(W)
    if ~opt.reverseMode
        wc   = vertcat(W.cells);
        pw   = p(wc);
        rhos = [f.rhoWS, f.rhoOS];
        bw   = {bW(wc), bO(wc)};
        mw   = {mobW(wc), mobO(wc)};
        s    = {sW, 1 - sW};

        wm = WellModel();
        [cqs, weqs, ctrleqs, wc, state.wellSol] = ....
           wm.computeWellFlux(model, W, wellSol, pBH, {qWs, qOs}, ...
           pw, rhos, bw, mw, s, {}, 'nonlinearIteration', opt.iteration);
        eqs(4:5) = weqs;
        eqs{7} = ctrleqs;
        
        eqs{1}(wc) = eqs{1}(wc) - cqs{2};
        eqs{2}(wc) = eqs{2}(wc) - cqs{1};
        
        % Surfactant well equations
        [~, wciSurf, iInxW] = getWellSurfactant(W);
        cw        = c(wc);
        cw(iInxW) = wciSurf;
        
        % Divide away water mobility and add in surfactant
        bWqS = cw.*cqs{1};
        eqs{3}(wc) = eqs{3}(wc) - bWqS;
        
        eqs{6} = qWs.*cw - qWSurf;
        
        names(4:7) = {'oilWells', 'waterWells', 'surfactantWells', ...
           'closureWells'};
        types(4:7) = {'perf', 'perf', 'perf', 'well'};
    else
        % in reverse mode just gather zero-eqs of correct size
        for eqn = 4:7
            nw = numel(state0.wellSol);
            zw = double2ADI(zeros(nw,1), p0);
            eqs(4:7) = {zw, zw, zw, zw};
        end
        names(4:7) = {'empty', 'empty', 'empty', 'empty'};
        types(4:7) = {'none', 'none', 'none', 'none'};
    end
end
problem = LinearizedProblem(eqs, types, names, primaryVars, state, dt);
problem.iterationNo = opt.iteration;
end

%--------------------------------------------------------------------------


function [wSurf, wciSurf, iInxW] = getWellSurfactant(W)
    if isempty(W)
        wSurf = [];
        wciSurf = [];
        iInxW = [];
        return
    end
    inj   = vertcat(W.sign)==1;
    surfInj = cellfun(@(x)~isempty(x), {W(inj).surf});
    wSurf = zeros(nnz(inj), 1);
    wSurf(surfInj) = vertcat(W(inj(surfInj)).surf);
    wciSurf = rldecode(wSurf, cellfun(@numel, {W(inj).cells}));
    
    % Injection cells
    nPerf = cellfun(@numel, {W.cells})';
    nw    = numel(W);
    perf2well = rldecode((1:nw)', nPerf);
    compi = vertcat(W.compi);
    iInx  = rldecode(inj, nPerf);
    iInx  = find(iInx);
    iInxW = iInx(compi(perf2well(iInx),1)==1);
end