NOECHO

-- Schedule include file

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'A'    'WATER' 'OPEN'  'BHP'   2*              320/
     'B'    'WATER' 'OPEN'  'BHP'   2*              320/
	 'C'    'WATER' 'OPEN'  'BHP'   2*              320/
     'D'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'A'   0.0 /
  'B'   0.0 /  
  'C'   0.0 /
  'D'   0.0 /  
/

TSTEP
5*1 10*2 100*10
/



WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'A'    'WATER' 'OPEN'  'BHP'   2*              320/
     'B'    'WATER' 'OPEN'  'BHP'   2*              320/
	 'C'    'WATER' 'OPEN'  'BHP'   2*              320/
     'D'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'A'   50.0 /
  'B'   0.0 /  
  'C'   0.0 /
  'D'   0.0 /  
/

TSTEP
5*1 10*2 50*10
/

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'A'    'WATER' 'OPEN'  'BHP'   2*              320/
     'B'    'WATER' 'OPEN'  'BHP'   2*              320/
	 'C'    'WATER' 'OPEN'  'BHP'   2*              320/
     'D'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'A'   0.0 /
  'B'   50.0 /  
  'C'   0.0 /
  'D'   0.0 /  
/

TSTEP
5*1 10*2 50*10
/

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'A'    'WATER' 'OPEN'  'BHP'   2*              320/
     'B'    'WATER' 'OPEN'  'BHP'   2*              320/
	 'C'    'WATER' 'OPEN'  'BHP'   2*              320/
     'D'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'A'   0.0 /
  'B'   0.0 /  
  'C'   50.0 /
  'D'   0.0 /  
/

TSTEP
5*1 10*2 50*10
/

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'A'    'WATER' 'OPEN'  'BHP'   2*              320/
     'B'    'WATER' 'OPEN'  'BHP'   2*              320/
	 'C'    'WATER' 'OPEN'  'BHP'   2*              320/
     'D'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'A'   0.0 /
  'B'   0.0 /  
  'C'   0.0 /
  'D'   50.0 /  
/

TSTEP
5*1 10*2 50*10
/

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'A'    'WATER' 'OPEN'  'BHP'   2*              320/
     'B'    'WATER' 'OPEN'  'BHP'   2*              320/
	 'C'    'WATER' 'OPEN'  'BHP'   2*              320/
     'D'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'A'   0.0 /
  'B'   0.0 /  
  'C'   0.0 /
  'D'   0.0 /  
/

TSTEP
5*1 10*2 50*10
/


ECHO