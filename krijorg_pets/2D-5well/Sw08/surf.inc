NOECHO
-- Surfactant properties (keywords starting with SURF)

-- SURFST: Oil-water IFT in the presence of surfactant
SURFST
--
-- Concentration        Oil-water IFT
--      KG/SM3              N/m
        0				0.05
        0.1				0.0005
        0.5				1.00E-05
        1				1.00E-06
        30				1.00E-06
        100				1.00E-06/
        0				0.05
        0.1				0.0005
        0.5				1.00E-05
        1				1.00E-06
        30				1.00E-06
        100				1.00E-06/         




-- SURFVISC: surfactant mixture viscosity with different concentrations.
SURFVISC
--
--  Concentration       Viscosity
--
		0				0.61
		30				0.8
		100				1/
		0				0.61
		30				0.8
		100				1/




-- SURFCAPD: Capillary de-saturation curve for surfactant. Describes transition from immiscible to miscible regimes.
SURFCAPD
--  Log10(CAPN)         Miscibility
--  
		-10				0
		-5.5			0
		-4				0.5
		-3				1
		2				1/
		-10				0
		-5.5			0
		-4				0.5
		-3				1
		2				1/



-- SURFADS: Surfacant adsorption table
SURFADS
-- Surfactant       Adsorbed mass
-- concentration    
-- KG/SM3               KG/KG
	0					0
	1					0.0005
	30					0.0005
	100					0.0005/
	0					0
	1					0.0005
	30					0.0005
	100					0.0005/	



-- SURFROCK: Rock properties in surfactant model
SURFROCK
-- Index        Density
    1            2650/
    1            2650/

ECHO